# Service Order
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render

from src.models import Appointment


@login_required
def appointment_list(request):
    service_orders=Appointment.objects.all()

    page = request.GET.get('page')
    paginator = Paginator(service_orders, 10)

    try:
        service_orders = paginator.page(page)

    except PageNotAnInteger:
        service_orders = paginator.page(1)

    except EmptyPage:
        service_orders = paginator.page(paginator.num_pages)

    return render(request,"admin_site/appointments/appointment_list.html",{'objects':service_orders})