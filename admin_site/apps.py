from django.apps import AppConfig

from admin_site.signals import my_handler


class AdminSiteConfig(AppConfig):
    name = 'admin_site'
    verbose_name='Admin Site'

    def ready(self):
        import admin_site.signals
