from django.shortcuts import render

# Create your views here.
from notifications.views import JsonResponse

from src.models import ServiceType, Appointment


def home(request):
    services=ServiceType.objects.all()
    return render(request,"src/base.html",{'services':services})

def about_us(request):
    services=ServiceType.objects.all()
    return render(request,"src/about_us.html",{'services':services})

def services(request):
    services=ServiceType.objects.all()
    return render(request,"src/services.html",{'services':services})

def gallery(request):
    services=ServiceType.objects.all()
    return render(request,"src/gallery.html",{'services':services})

def contact_us(request):
    services=ServiceType.objects.all()
    return render(request,"src/contact_us.html",{'services':services})

def login(request):
    return render(request,"account/login.html")

def error_404(request):
    data={}
    return render(request,"src/404.html",data)

def appointment(request):
    print(request.POST)
    try:
        appointment=Appointment()
        appointment.phone=request.POST['Phone']
        appointment.date=request.POST['Date']
        appointment.attendance=request.POST['Party']
        appointment.email=request.POST['Email']
        appointment.time=request.POST['Time']
        appointment.msg=request.POST['DaMessagete']
        appointment.save()
        return JsonResponse({"success":"ok"})
    except BaseException as e:
        return JsonResponse({"error":e})
