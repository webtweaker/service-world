from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from notifications.models import Notification
from django.http.response import HttpResponse, JsonResponse

@login_required
def dashboard(request):
    return render(request,"admin_site/base.html",{})

def notifications_count(request):
    data = {}
    data['noti_count'] = Notification.objects.unread().count()

    return JsonResponse(data)